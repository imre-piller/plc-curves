Experimental Classification
---------------------------

We have classified the mentioned 28 curves into the following categories.

* *no*: the PLC effect is not significant
* *minimal*: there are some little jumps of the curve
* *peak*: visible peaks in a periodical arrangement
* *noisy*: the curve contains high frequency noise

There are some curves which has categorized into the minimal-peak or the peak-noisy classes at the same time. From this reason we get the following cardinality of the classes:

* *no*: 5
* *minimal*: 11
* *peak*: 15
* *noisy*: 5

Polynomial approximation
------------------------

The measured curve data is rather a point cloud than a continuous curve. It means that the strain values are not necessarily ordered. The polynomial regression provides us a proper approximation. We have used the classical polynomial regression by degree 5. After, we have subtracted the resulted curve (as the estimation of the original flow curve) from the measurements. In the followings, this curve considered as the PLC curve.

The resulted PLC curves still have some ordering problem. As a rough estimation we have calculated the euclidean distances of the neighbour values. These distances show the absolute measure of the changes of amplitude.

Feature Extraction and Classification
-------------------------------------

At the next step, we have calculated feature values from the distance values. We have considered the following features:

* number of the measured points,
* minimal, maximal values of the dimensions, and their ranges,
* maximal distance value,
* average distance.

We have plotted the average distances by maximal distances for making the classes visible. It shows that these values results similar classification as our experimental classification method. In the next Figure we can show our 28 curves. The horizontal axis is the average distance and the vertical is the maximal distance. The color of the dots denote the classes of the experimental classification (no=green, minimal=blue, peak=red, noisy=black).

As we can see, it makes possible to distinguish the no and minimal cases from peaked and noisy curves in most cases.

