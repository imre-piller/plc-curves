Clustering and Classification of PLC Curves
===========================================

This research aims to develop a method for classifying the different types of PLC effect automatically. It requires some kind of mathematical foundation which based on exact analytical model instead of statistical and heuristical approaches.

We have chosen PLC types for classification experimentally at first. After, we tried to extract features and determined the types by clustering algorithms.

Available Measurements
----------------------

We have 28 curves for analysing the PLC effect. We have introduced a simple naming convention for identifying them. Their names contains the material and the temperature.

Extraction of the PLC Effect
----------------------------

We can consider the measured strain-stress curves as the sum of the flow curve and the curve of the additional PLC effect. Therefore, we would like to subtract the flow curve from the measurements.

The calculation of the flow curve is possible but it is a complicated and time consuming operation. Moreover, the measurements consist noises which should be removed. From these reasons, we try to estimate the flow curve by polinomial curve fitting method.

Estimation of the Strain Range
------------------------------

At low and high strain rates there are some invalid stress values. It primarily comes from the measurement method. For better flow curve approximation we have to denote an interval on strain dimension.

* We have determined the appropriate minimal and maximal strain values experimentally.
* The maximal strain has defined at the strain of the first maximal stress value. When it does not provide the expected maximal value, we can use moving average for reducing the PLC effect or the noise.
* The minimal strain value can be estimated from the distance of the measured points. There are some larger distance at the beginning of the samples.

Extraction of PLC Features
--------------------------

It is enough to consider the curve of the PLC effect without the flow curve. We find the peaks of the curve which provides the following values:

* strain value of the peak center,
* minimal stress value,
* maximal stress value.

It provides the following features:

* number of peaks,
* average peak size (as the distance between minimal and maximal stress value),
* average distance between the peaks.

