#ifndef CURVE_H
#define CURVE_H

#include <QPointF>
#include <QRectF>
#include <QSizeF>
#include <QString>
#include <QVector>

class Curve
{
public:
    Curve();

    /**
     * @brief Load the curve points from the data file
     * @param path path of the data file
     */
    void loadValues(const QString& path);

    /**
     * @brief Calculate the range of the values as the size of the interval
     * @return a size object
     */
    QSizeF calcRange();

    /**
     * @brief Calculate the minimum of the values.
     * @return a point
     */
    QPointF calcMinimumPoint();

    QVector<QPointF> _points;
};

#endif // CURVE_H
