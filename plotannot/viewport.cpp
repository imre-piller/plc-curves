#include "viewport.h"

#include <QDebug>

ViewPort::ViewPort()
{
}

void ViewPort::setCurve(Curve *curve)
{
    _curve = curve;
}

void ViewPort::setCorners(const QPointF &view1, const QPointF &view2)
{
    _view1 = view1;
    _view2 = view2;
}

void ViewPort::setWindowSize(const QSizeF &windowSize)
{
    _windowSize = windowSize;
}

QVector<QPointF> ViewPort::getPoints() const
{
    QVector<QPointF> points;
    for (const QPointF& point : _curve->_points) {
        points.append(calcPlotToScreen(point));
    }
    return points;
}

QPointF ViewPort::calcPlotToScreen(const QPointF &point) const
{
    // double rX = point.x() - _view1.x();
    // double rY = point.y() - _view1.y();
    double ratioX = (point.x() - _view1.x()) / (_view2.x() - _view1.x());
    double ratioY = (point.y() - _view1.y()) / (_view2.y() - _view1.y());
    double screenX = ratioX * _windowSize.width();
    double screenY = (1 - ratioY) * _windowSize.height();
    return QPointF(screenX, screenY);
}

QPointF ViewPort::calcScreenToPlot(const QPointF &point) const
{
    double ratioX = point.x() / _windowSize.width();
    double ratioY = 1.0 - (point.y() / _windowSize.height());
    double plotX = _view1.x() + (ratioX * (_view2.x() - _view1.x()));
    double plotY = _view1.y() + (ratioY * (_view2.y() - _view1.y()));
    return QPointF(plotX, plotY);
}

void ViewPort::shift(const QPointF &source, const QPointF &target)
{
    QPointF s = calcScreenToPlot(source);
    QPointF t = calcScreenToPlot(target);
    QPointF diff = s - t;
    _view1 += diff;
    _view2 += diff;
}

void ViewPort::zoom(const QPointF &center, double factor)
{
    qDebug() << "view" << _view1 << _view2;
    QPointF p = calcScreenToPlot(center);
    qDebug() << "p" << p;
    qDebug() << "factor" << factor;
    _view1 = p + ((_view1 - p) * factor);
    _view2 = p + ((_view2 - p) * factor);
}

int ViewPort::findSelectedIndex(const QPointF &point) const
{
    double minDistance;
    QPointF p = calcScreenToPlot(point);
    int index = 0;
    int nPoints = _curve->_points.size();
    for (int i = 0; i < nPoints; ++i) {
        double distance = calcDistance(p, _curve->_points[i]);
        if (distance < minDistance) {
            minDistance = distance;
            index = i;
        }
    }
    return index;
}

double ViewPort::calcDistance(const QPointF &a, const QPointF &b) const
{
    QPointF diff = a - b;
    // NOTE: It calculate only the square of the distance!
    double distance = diff.x() * diff.x() + diff.y() * diff.y();
    return distance;
}
