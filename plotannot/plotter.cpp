#include "plotter.h"

#include <QDebug>

#include <QApplication>
#include <QMouseEvent>
#include <QPainter>
#include <QPen>
#include <QPointF>
#include <QWheelEvent>

#include <fstream>

Plotter::Plotter(QWidget *parent) : QWidget(parent)
{
    loadExtremas("");
}

void Plotter::mousePressEvent(QMouseEvent *event)
{
    _referencePoint = QPointF(event->pos());
    if (event->button() == Qt::RightButton) {
        QPointF point = _viewPort.calcScreenToPlot(QPointF(event->pos()));
        if (QApplication::keyboardModifiers().testFlag(Qt::ShiftModifier) == false) {
            _extremas.push_back(point);
        }
        else {
            double minDistance = 1e16;
            int i = 0;
            int minIndex;
            for (const QPointF& extrema : _extremas) {
                QPointF diff = extrema - point;
                double distance = diff.x() * diff.x() + diff.y() * diff.y();
                if (distance < minDistance) {
                    minIndex = i;
                    minDistance = distance;
                }
                ++i;
            }
            _extremas.remove(minIndex);
        }
        repaint();
    }
    else if (event->button() == Qt::MiddleButton) {
        saveExtremas();
    }
}

void Plotter::mouseMoveEvent(QMouseEvent *event)
{
    QPointF point(event->pos());
    _viewPort.shift(_referencePoint, point);
    _referencePoint = point;
    repaint();
}

void Plotter::mouseReleaseEvent(QMouseEvent *event)
{
}

void Plotter::wheelEvent(QWheelEvent *event)
{
    double factor = 1.0 + ((double)event->delta() / 1000);
    _viewPort.zoom(event->posF(), factor);
    repaint();
}

void Plotter::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);

    _viewPort.setWindowSize(this->size());

    QPen pen(QColor(255, 0, 0), 0);
    painter.setPen(pen);

    QVector<QPointF> points = _viewPort.getPoints();

    int nPoints = points.size();
    for (int i = 0; i < nPoints - 1; ++i) {
        painter.drawLine(points[i], points[i + 1]);
    }

    QPen extremaPen(QColor(0, 0, 200), 0);
    painter.setPen(extremaPen);
    for (const QPointF& extrema : _extremas) {
        QPointF center = _viewPort.calcPlotToScreen(extrema);
        painter.drawEllipse(center, 8, 8);
    }
}

void Plotter::setCurve(Curve *curve)
{
    _curve = curve;
    _viewPort.setCurve(_curve);
    _viewPort.setCorners(QPointF(0, 0), QPointF(0.3, 1000));
}

void Plotter::loadExtremas(const QString &path)
{
    // TODO: Use the path!
    std::ifstream extremaFile("sample.peaks");
    if (extremaFile.is_open() == false) {
        throw std::invalid_argument("Unable to open extrema file!");
    }
    double x, y;
    _extremas.clear();
    while (extremaFile >> x >> y) {
        _extremas.push_back(QPointF(x, y));
    }
}

void Plotter::saveExtremas()
{
    QFile extremaFile("sample.peaks");
    if (extremaFile.open(QIODevice::ReadWrite)) {
        QTextStream stream(&extremaFile);
        for (const QPointF& extrema : _extremas) {
            stream << extrema.x() << " " << extrema.y() << "\n";
        }
        extremaFile.close();
        qDebug() << "The extremas have saved to file!";
    }
    else {
        qDebug() << "Unable to open extrema file for writing!";
    }
}
