#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "curve.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Curve _curve;
};

#endif // MAINWINDOW_H
