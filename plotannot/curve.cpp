#include "curve.h"

#include <QDebug>

#include <exception>
#include <fstream>

Curve::Curve()
{
}

void Curve::loadValues(const QString &path)
{
    std::ifstream dataFile(path.toStdString());
    if (dataFile.is_open() == false) {
        throw std::invalid_argument("Unable to find the data file!");
    }
    double x, y;
    while (dataFile >> x >> y) {
        _points.append(QPointF(x, y));
    }
}

QSizeF Curve::calcRange()
{
    double maxX = 0.0;
    double maxY = 0.0;
    QPointF minPoint = calcMinimumPoint();
    for (const QPointF& point : _points) {
        double distX = point.x() - minPoint.x();
        double distY = point.y() - minPoint.y();
        if (distX > maxX) {
            maxX = distX;
        }
        if (distY > maxY) {
            maxY = distY;
        }
    }
    return QSizeF(maxX, maxY);
}

QPointF Curve::calcMinimumPoint()
{
    double minX, minY;
    minX = _points[0].x();
    minY = _points[0].y();
    for (int i = 1; i < _points.size(); ++i) {
        if (_points[i].x() < minX) {
            minX = _points[i].x();
        }
        if (_points[i].y() < minY) {
            minY = _points[i].y();
        }
    }
    return QPointF(minX, minY);
}
