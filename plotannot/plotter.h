#ifndef PLOTTER_H
#define PLOTTER_H

#include "curve.h"
#include "viewport.h"

#include <QObject>
#include <QVector>
#include <QWidget>

class Plotter : public QWidget
{
    Q_OBJECT
public:
    explicit Plotter(QWidget *parent = 0);

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent *event);

    void paintEvent(QPaintEvent *event);
    void setCurve(Curve* curve);

    void loadExtremas(const QString& path);
    void saveExtremas();

signals:

public slots:

private:

    ViewPort _viewPort;
    Curve* _curve;
    QPointF _referencePoint;
    QVector<QPointF> _extremas;
};

#endif // PLOTTER_H
