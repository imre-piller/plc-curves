#ifndef VIEWPORT_H
#define VIEWPORT_H

#include "curve.h"

#include <QPointF>
#include <QSizeF>
#include <QVector>

class ViewPort
{
public:

    ViewPort();

    /**
     * @brief Set the curve of the viewport.
     * @param curve pointer to a curve
     */
    void setCurve(Curve* curve);

    /**
     * @brief Set the corners of the viewpoints.
     * @param view1 bottom left corner
     * @param view2 top right corner
     */
    void setCorners(const QPointF& view1, const QPointF& view2);

    /**
     * @brief Set the size of the window.
     * @param windowSize size of the window
     */
    void setWindowSize(const QSizeF& windowSize);

    /**
     * @brief Get the points in screen coordinates.
     * @return vector of points
     */
    QVector<QPointF> getPoints() const;

    /**
     * @brief Calculate from plot coordinate to screen coordinate.
     * @param point position in plot coordinates
     * @return position in screen coordinates
     */
    QPointF calcPlotToScreen(const QPointF& point) const;

    /**
     * @brief Calculate from screen coordinate to plot coordinate.
     * @param point position in screen coordinates
     * @return position in plot coordinates
     */
    QPointF calcScreenToPlot(const QPointF& point) const;

    /**
     * @brief Shift the viewport from source to target position.
     * @param source point
     * @param target point
     */
    void shift(const QPointF& source, const QPointF& target);

    /**
     * @brief Zoom the viewport to the given point.
     * @param center center of the zoom
     * @param factor zooming factor
     */
    void zoom(const QPointF& center, double factor);

    /**
     * @brief Find the index of the selected points.
     * @param point the clicked point
     * @return index of the selected point of the plot
     */
    int findSelectedIndex(const QPointF& point) const;

    /**
     * @brief Calculate the distance of the points.
     * @param a point
     * @param b point
     * @return euclidean distance
     */
    double calcDistance(const QPointF& a, const QPointF& b) const;

private:

    Curve* _curve;

    QPointF _view1;
    QPointF _view2;
    QSizeF _windowSize;
};

#endif // VIEWPORT_H
