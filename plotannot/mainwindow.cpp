#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    _curve.loadValues("curve.txt");

    qDebug() << "offset" << _curve.calcMinimumPoint();
    qDebug() << "range" << _curve.calcRange();

    ui->setupUi(this);

    ui->widget->setCurve(&_curve);
}

MainWindow::~MainWindow()
{
    delete ui;
}
